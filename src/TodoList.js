import React from "react";

const TodoList = ({ todos, deleteTodo }) => {
  const todoItem = todos;
  return (
    <div className="todos collection">
      {todos && todos.length ? (
        todos.map(todoItem => {
          return (
            <div className="collection-item" key={todoItem.id}>
              <span onClick={() => deleteTodo(todoItem.id)}>
                {todoItem.content}
              </span>
            </div>
          );
        })
      ) : (
        <div>
          {" "}
          <h4> No items in the list</h4>
        </div>
      )}
    </div>
  );
};

export default TodoList;
