import React, { Component } from "react";
import TodoList from "./TodoList";
import AddForm from "./AddForm";
class App extends Component {
  state = {
    todos: [
      { id: 1, content: "Learn react" },
      { id: 2, content: "Learn node" },
      { id: 3, content: "Learn java" },
      { id: 4, content: "Learn english" }
    ]
  };

  deleteTodo = id => {
    const newTodos = this.state.todos.filter(todo => todo.id !== id);
    this.setState({
      todos: newTodos
    });
  };

  addToDo = todo => {
    todo.id = Math.random();
    let todos = [...this.state.todos, todo];
    this.setState({
      todos
    });
  };
  render() {
    return (
      <div className="App">
        <h1 className=" center blue-text"> Todo List</h1>
        <TodoList todos={this.state.todos} deleteTodo={this.deleteTodo} />
        <AddForm addTodoItem={this.addToDo} />
      </div>
    );
  }
}

export default App;
