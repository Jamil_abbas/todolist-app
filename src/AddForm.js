import React, { Component } from "react";

class AddForm extends Component {
  state = {
    content: ""
  };
  handleChange = e => {
    this.setState({
      content: e.target.value
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.addTodoItem(this.state);
    this.setState({
      content: ""
    });
  };
  render() {
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <label>Add new TODO:</label>
          <input
            type="text"
            onChange={e => this.handleChange(e)}
            value={this.state.content}
          />
        </form>
      </div>
    );
  }
}

export default AddForm;
